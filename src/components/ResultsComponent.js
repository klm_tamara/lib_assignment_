import React, {useState, useEffect} from 'react';
import placeholder from '../resources/placeholder.png';
import {Loading} from './LoadingComponent';

const Results = ({match}) => {
  const [results, setResults] = useState({});
  const [page, setPage] = useState(1);
  const [maxPages, setMaxPages] = useState(1);
  const [error, setError] = useState(null);
  const [isloading, setIsLoading] = useState(true);

  const searchText = match.params.search;

  useEffect(() => {
    setIsLoading(true);
    //fetch(`https://api.itbook.store/1.0/search/${searchText}/${page}`)
    fetch(
      `https://openlibrary.org/search.json?title=${searchText}&page=${page}`,
    )
      .then(res => res.json())
      .then(res => setResults(res))
      .then(() => setIsLoading(false))
      .catch(err => setError(err));
  }, [searchText, page]);
  useEffect(() => {
    setPage(1);
  }, [searchText]);

  useEffect(() => {
    setMaxPages(results.num_found ? Math.ceil(results.num_found / 100) : 0);
  }, [results.num_found]);

  const Books = ({books}) =>
    books.map(item => {
      return (
        <div key={item.key} className="dt_results">
          <Book book={item} />
        </div>
      );
    });

  const Book = ({book}) => (
    <div className="dr_flex">
      <div className="num_fl">
        {book.cover_i ? (
          <img
            src={`https://covers.openlibrary.org/b/id/${book.cover_i}-M.jpg`}
            alt={book.title}
            className="pic_1"
          />
        ) : (
          <img src={placeholder} alt="book" />
        )}
      </div>
      <div className="main_cont_res">
        <p>
          <strong>Title: </strong> {book.title}
        </p>
        {book.first_sentence && (
          <p>
            <strong>First sentence: </strong> {book.first_sentence}
          </p>
        )}

        {/*<p>
          <strong>Author: </strong>
          {book.author_name[0]}
        </p>*/}
      </div>
    </div>
  );

  const Pagination = () => {
    if (maxPages === 1 || !results.docs || results.docs.length === 0) {
      return null;
    }
    return (
      <div>
        {page !== 1 && (
          <div
            className="pagination_new previous"
            onClick={() => setPage(page - 1)}>
            prev
          </div>
        )}
        <div className="num">
          {page} / {maxPages}
        </div>
        {page !== maxPages && (
          <div
            className="pagination_new next"
            onClick={() => setPage(page + 1)}>
            next
          </div>
        )}
      </div>
    );
  };
  if (isloading) {
    return (
      <div className="dt_results">
        <Loading />
      </div>
    );
  } else if (error) {
    return error;
  } else {
    return (
      <div>
        <div className="dt_results">
          <p className="total">
            <strong>Total results: </strong> {results.num_found}{' '}
          </p>
          <br />
          <Pagination />
        </div>
        <br />
        <Books books={results.docs ? results.docs : []} />
        <div className="dt_results">
          <br />
          <Pagination />
        </div>
        <br />
      </div>
    );
  }
};

export default Results;
