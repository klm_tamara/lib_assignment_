import React from 'react';
import Nav from './NavComponent';
import FrontContainers from './FrontContainers';
import SearchUDK from './SearchUDK';
import LogIn from './LoginComponent';
import SearchDictionary from './SearchDictionary';
import Results from './ResultsComponent';
import SearchField from './SearchField';
import {Switch, Route, Redirect} from 'react-router-dom';
function Main() {
  return (
    <div>
      <Nav />
      <SearchField />
      <Switch>
        <Route path="/home">
          <FrontContainers />
        </Route>
        <Route exact path="/SearchUDK">
          <SearchUDK />
        </Route>
        <Route exact path="/SearchDictionary">
          <SearchDictionary />
        </Route>
        <Route exact path="/Login">
          <LogIn />
        </Route>
        <Route path="/results/:search" component={Results} />
        <Redirect to="/home" />
      </Switch>
    </div>
  );
}

export default Main;
