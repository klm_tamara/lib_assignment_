import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import it_books from '../resources/it-books_.png';
import hat from '../resources/Hat_.png';

function SearchField() {
  const [value, setValue] = useState('');
  const changeValue = newValue => {
    setValue(newValue);
  };

  return (
    <div className="main_header">
      <div className="main_header_cont">
        <div className="flex_center_cont">
          <div className="flex_center_item">
            <div className="flex_row_cont">
              <div className="flex_main_pic">
                <Link to="/home">
                  <img src={hat} alt="Logo" className="mim_medium_crown" />
                  <img src={it_books} alt="Name" className="mim_library" />
                </Link>
              </div>
              <div className="flex_main_inputfield">
                <form id="myForm">
                  <div className="flex_input_fields">
                    <div className="rem_spc">
                      <input
                        className="main_search_field"
                        type="text"
                        value={value}
                        name="find"
                        placeholder="Search "
                        id="mainsearchfield"
                        onChange={e => {
                          changeValue(e.target.value);
                        }}
                      />
                    </div>
                    <div className="rem_spc2">
                      <Link to={`/results/${value}`}>
                        <button
                          type="submit"
                          name="submit"
                          className="search_btn">
                          <i className="fa fa-search"></i>
                        </button>
                      </Link>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SearchField;
