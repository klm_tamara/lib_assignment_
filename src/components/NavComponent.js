import React from 'react';
import {NavLink} from 'react-router-dom';

function Nav() {
  return (
    <header>
      <div className="stickynav">
        <div className="toggle">
          <div className="navbar-toggle"></div>
        </div>
        <nav>
          <ul className="navigation" id="topnav">
            <li>
              <NavLink to="/home" activeClassName="current_link">
                <span className="fa fa-server"></span>&nbsp; Стандартний пошук
              </NavLink>
            </li>
            <li>
              <NavLink to="/SearchDictionary" activeClassName="current_link">
                <span className="fa fa-book"></span>&nbsp; За словником
              </NavLink>
            </li>
            <li>
              <NavLink to="/SearchUDK" activeClassName="current_link">
                <span className="fa fa-folder"></span>&nbsp; УДК-навігатор
              </NavLink>
            </li>
            <li>
              <NavLink to="/Login" activeClassName="current_link">
                <span className="fa fa-sign-in fa-jg"></span>&nbsp; Log in
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default Nav;
