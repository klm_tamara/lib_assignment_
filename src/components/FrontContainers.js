import React from 'react';

function FrontContainers() {
  return (
    <div className="main_body">
      <div className="flex_body_sthtoshow" id="sth_div">
        <div className="containertoshow jcenter">
          <div className="containertoshow_flex_j">
            <h2>Euromonitor</h2>
            <hr className="style-two" />
            <p>
              Глобальна система ділової інформації компанії Euromonitor
              International, що сприяє бізнес-плануванню та прийняттю
              стратегічних рішень. Passport забезпечує єдиний доступ до
              відомостей про галузі промисловості, країни та споживачів.
            </p>
            <h3>Зміст</h3>
            <ul className="dashed ltp">
              <li>
                статистика, аналітика та огляди світового ринку основних
                товарів;
              </li>
              <li>рейтинги і профілі компаній провідних країн та галузей;</li>
              <li>
                повна база джерел інформації основних галузей промисловості та
                країн світу.
              </li>
              <li>та інше</li>
            </ul>
          </div>
          <div className="containertoshow_flex_j">
            <button className="wthsth">Visit Euromonitor</button>
          </div>
        </div>
        <div className="containertoshow jcenter">
          <div className="containertoshow_flex_j">
            <h2>EBSCOhost</h2>
            <hr className="style-two" />
            <p>
              Повнотекстові і реферативні науково-технічні, економічні та
              довідкові бази даних фірми EBSCO PUBLISHING. БД EBSCO – доступ до
              колекції, що нараховує більше 6000 періодичних видань, які
              розподілені серед різних баз даних системи.
            </p>
            <ul className="ltp">
              <li>
                <strong>Business Source Premier</strong> - найпопулярніша база
                даних з дослідження в галузі бізнесу, охоплює практично усі
                предметні галузі, пов’язані з бізнесом, включаючи маркетинг,
                менеджмент, систему управлінської інформації, керівництва з
                організації проектів, бухгалтерський облік, фінанси та економіку
              </li>
              <li>
                <strong>Academic Search Premier</strong> - мультидисциплінарна
                база даних
              </li>
              <li>
                <strong>ERIC</strong> - інформаційний центр освітніх ресурсів
              </li>
              <li>Та інше</li>
            </ul>
          </div>
          <div className="containertoshow_flex_j">
            <button className="wthsth">Visit EBSCO</button>
          </div>
        </div>
        <div className="containertoshow jcenter">
          <div className="containertoshow_flex_j">
            <h2>Additional</h2>
            <hr className="style-two" />
            <ul className="dashed ltp">
              <li>
                <a href="https://www.ft.com/">Financial Times</a>
              </li>
              <li>
                <a href="https://www.strategybusiness.ru/jour">
                  Журнал "Стратегії"
                </a>
              </li>
              <li>
                <a href="http://www.management.com.ua/announce.php?edition=17">
                  Журнал "Фінансист"
                </a>
              </li>
            </ul>
            <h2>Our Website</h2>
            <hr className="style-two" />
            <p>
              У Бізнес-школі МІМ регулярно проводяться заходи різного галузевого
              і професійного спрямування: семінари, ворк-шопи, майстерні
              бізнесу, конференції, круглі столи, а також зустрічі із лідерами
              думок та видатними особистостями сучасності.
            </p>
          </div>
          <div className="containertoshow_flex_j">
            <button className="wthsth">Visit Our Website</button>
          </div>
        </div>
      </div>
      <br />
    </div>
  );
}

export default FrontContainers;
